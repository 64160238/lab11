package com.charudet.lab11;

public class Submarine extends Vahicle implements Swimable{

	public Submarine(String name, String enginename) {
		super(name, enginename);
	}

	@Override
	public void swim() {
		System.out.println(this + " swim.");
	}

	@Override
	public void Vahicle() {
		System.out.println(this + " Vahicle.");
	}

	@Override
	public String toString() {
		return "Submarine("+this.getName()+")";
	}
	
}
