package com.charudet.lab11;

public class Plane extends Vahicle implements Flyable{

	public Plane(String name, String enginename) {
		super(name, enginename);
	}

	@Override
	public void fly() {
		System.out.println(this + " fly.");
	}

	@Override
	public void landing() {
		System.out.println(this + " landing.");
	}

	@Override
	public void takeoff() {
		System.out.println(this + " takeoff.");
	}

	@Override
	public void Vahicle() {
		System.out.println(this + " Vahicle.");
	}

	@Override
	public String toString() {
		return "Plane("+this.getName()+")";
	}
	
}
