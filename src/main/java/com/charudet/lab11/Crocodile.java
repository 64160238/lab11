package com.charudet.lab11;

public class Crocodile extends Animal implements Crawlable, Swimable{
	public Crocodile(String name, int numberOfLeg) {
		super(name, 4);
	}

	@Override
	public void crawl() {
		System.out.println(this + " crawl.");
	}

	@Override
	public void eat() {
		System.out.println(this + " eat.");
	}

	@Override
	public void sleep() {
		System.out.println(this + " sleep.");
	}

	@Override
	public String toString() {
		return "Crocodile("+this.getName()+")";
	}

	@Override
	public void swim() {
		System.out.println(this + " swim.");
	}
}
