package com.charudet.lab11;

public interface Walkable {
	public void walk();
	public void run();
}
