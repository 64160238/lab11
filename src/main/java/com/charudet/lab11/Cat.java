package com.charudet.lab11;

public class Cat extends Animal implements Walkable{
	public Cat(String name) {
		super(name, 4);
	}

	@Override
	public void eat() {
		System.out.println(this + " eat.");
	}

	@Override
	public void sleep() {
		System.out.println(this + " sleep.");
	}

	@Override
	public String toString() {
		return "Cat("+this.getName()+")";
	}

	@Override
	public void walk() {
		System.out.println(this + " walk.");
	}

	@Override
	public void run() {
		System.out.println(this + " run.");
	}
}
