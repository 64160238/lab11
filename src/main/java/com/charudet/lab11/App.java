package com.charudet.lab11;

public class App 
{
    private static final String enginename = null;
    private static final String name = null;

    public static void main( String[] args )
    {
        Human human1 = new Human("Somchai");
        human1.eat();
        human1.sleep();
        human1.walk();
        human1.run();

        Rat rat1 = new Rat("Ratatouille");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();

        Dog dog1 = new Dog("Scooby");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();

        Cat cat1 = new Cat("Grafield");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();

        Bird bird1 = new Bird("Anggy", 2);
        bird1.eat();
        bird1.sleep();
        bird1.fly();
        bird1.takeoff();
        bird1.landing();

        Bat bat1 = new Bat("Black", 2);
        bat1.eat();
        bat1.sleep();
        bat1.fly();
        bat1.takeoff();
        bat1.landing();

        Snake snake1 = new Snake("Anaconda", 0);
        snake1.eat();
        snake1.sleep();
        snake1.crawl();

        Crocodile crocodile1 = new Crocodile("Chalawan", 4);
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.crawl();
        crocodile1.swim();

        Fish fish1 = new Fish("nemo", 0);
        fish1.eat();
        fish1.sleep();
        fish1.swim();

        Plane plane1 = new Plane(null, null);
        plane1.Vahicle();
        plane1.fly();
        plane1.takeoff();
        plane1.landing();

        Submarine submarine1 = new Submarine(null, null);
        submarine1.Vahicle();
        submarine1.swim();

        Walkable[] walkableObject = {human1, rat1, dog1, cat1};
        for(int i = 0; i < walkableObject.length; i++) {
            walkableObject[i].walk();
            walkableObject[i].run();
        }

        Flyable[] flyableObject = {bat1, bird1, plane1};
        for(int i = 0; i < flyableObject.length; i++) {
            flyableObject[i].fly();
            flyableObject[i].takeoff();
            flyableObject[i].landing();
        }

        Swimable[] swimableObject = {fish1, submarine1, crocodile1};
        for(int i = 0; i < swimableObject.length; i++) {
            swimableObject[i].swim();
        }

        Crawlable[] crawlablesObject = {snake1, crocodile1};
        for(int i = 0; i < swimableObject.length; i++) {
            crawlablesObject[i].crawl();
        }
    }
}
