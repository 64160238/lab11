package com.charudet.lab11;

public abstract class Vahicle {
	private String name;
	private String engineName;

	public Vahicle(String name, String enginename) {
		this.name = name;
		this.engineName = enginename;
	}

	public String getName() {
		return name;
	}

	public String getEngineName() {
		return engineName;
	}

	public void setName() {
		this.name = name;
	}

	public void setEngineName() {
		this.engineName = engineName;
	}

	@Override
	public String toString() {
		return "Vahicle (" + name + ") has " + engineName + "engine";
	}

	public abstract void Vahicle();
}
